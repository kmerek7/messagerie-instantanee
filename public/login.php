<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MsgInst</title> 
    <link rel="stylesheet" href="./assets/css/style.css">
    <link rel="stylesheet" href="./assets/js/index.js">
    <link rel="stylesheet" href="https://use.fontawesome.com/5ee0f63a65.css">

</head>
<body>
    <div class="wraper">
        <section class="form login">
            <header> Messagerie Instantanee</header>
            <form action="#">
                <div class="error-txt">Message d'erreur</div>
                    <div class="field input">
                        <label>Adresse Email</label>
                        <input type="text" placeholder="Entrez votre adresse email">
                    </div>
                    <div class="field input">
                        <label>Password</label>
                        <input type="password" placeholder="Entrez votre mots de passe">
                        <i class="fa fa-eye"></i>
                    </div>
                   
                    <div class="field button">
                        <input type="submit" value="Allez vers le chat">
                    </div>
            </form>
            <div class="link">Pas encore inscrit ? <a href="#">Inscrivez vous</a>
            </div>
        </section>
    </div>
</body>
<footer>
    <script src="./assets/js/main.js"></script>
</footer>
</html>