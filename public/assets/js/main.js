let pswrdField= document.querySelector(".form .field input[type='password'] "),
 toggleBtn= document.querySelector(".form .field i");

 let searchBar= document.querySelector(".users .search input"),
 searchBtn = document.querySelector(".users .search button");

 function pass() 
 {
     toggleBtn.onclick = ()=>
     {
         if (pswrdField.type == "password") 
         {
             pswrdField.type= "text";
             toggleBtn.classList.add("active");
            }
            else
            {
                pswrdField.type= "password";
                toggleBtn.classList.remove("active");
            } 
        }
    }
    
    function search() 
    {
        searchBtn.onclick = ()=>
        {
            searchBar.classList.toggle("active");
            searchBar.focus();
            searchBtn.classList.toggle("active");
        }
        
    }

    if (searchBtn == null & toggleBtn != null ) 
    {
        searchBtn=''
        toggleBtn.addEventListener('click',pass);
    }
    else
    {
        searchBtn.addEventListener('click',search);
    }
