<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MsgInst</title> 
    <link rel="stylesheet" href="./assets/css/style.css">
    <link rel="stylesheet" href="./assets/js/index.js">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- <link rel="stylesheet" href="https://use.fontawesome.com/5ee0f63a65.css"> -->

</head>
<body>
    <div class="wraper">
        <section class="app">
            <header>
                    <a href="#" class="back-icon"><i class="fa fa-arrow-left"></i></a>
                    <img src="./assets/img/avatar.png" alt="">
                    <div class="details">
                        <span>User</span>
                        <p>En ligne</p>
                    </div> 
            </header>
            <div class="chat-box">
                <div class="chat send">
                    <div class="details">
                    <p>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Nesciunt, optio maxime possimus sunt, a maiores ipsam enim animi quia exercitationem ipsum sapiente esse, molestiae qui assumenda hic distinctio expedita blanditiis.
                    </p>
                    </div>
                </div>
                <div class="chat receive">
                    <img src="./assets/img/avatar.png" alt="">
                    <div class="details">
                    <p>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Nesciunt, optio maxime possimus sunt, a maiores ipsam enim animi quia exercitationem ipsum sapiente esse, molestiae qui assumenda hic distinctio expedita blanditiis.
                    </p>
                    </div>
                </div>
                <div class="chat send">
                    <div class="details">
                    <p>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Nesciunt, optio maxime possimus sunt, a maiores ipsam enim animi quia exercitationem ipsum sapiente esse, molestiae qui assumenda hic distinctio expedita blanditiis.
                    </p>
                    </div>
                </div>
                <div class="chat receive">
                    <img src="./assets/img/avatar.png" alt="">
                    <div class="details">
                    <p>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Nesciunt, optio maxime possimus sunt, a maiores ipsam enim animi quia exercitationem ipsum sapiente esse, molestiae qui assumenda hic distinctio expedita blanditiis.
                    </p>
                    </div>
                </div>
                <div class="chat send">
                    <div class="details">
                    <p>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Nesciunt, optio maxime possimus sunt, a maiores ipsam enim animi quia exercitationem ipsum sapiente esse, molestiae qui assumenda hic distinctio expedita blanditiis.
                    </p>
                    </div>
                </div>
                <div class="chat receive">
                    <img src="./assets/img/avatar.png" alt="">
                    <div class="details">
                    <p>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Nesciunt, optio maxime possimus sunt, a maiores ipsam enim animi quia exercitationem ipsum sapiente esse, molestiae qui assumenda hic distinctio expedita blanditiis.
                    </p>
                    </div>
                </div>
                <div class="chat send">
                    <div class="details">
                    <p>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Nesciunt, optio maxime possimus sunt, a maiores ipsam enim animi quia exercitationem ipsum sapiente esse, molestiae qui assumenda hic distinctio expedita blanditiis.
                    </p>
                    </div>
                </div>
                <div class="chat receive">
                    <img src="./assets/img/avatar.png" alt="">
                    <div class="details">
                    <p>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Nesciunt, optio maxime possimus sunt, a maiores ipsam enim animi quia exercitationem ipsum sapiente esse, molestiae qui assumenda hic distinctio expedita blanditiis.
                    </p>
                    </div>
                </div>
                <div class="chat send">
                    <div class="details">
                    <p>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Nesciunt, optio maxime possimus sunt, a maiores ipsam enim animi quia exercitationem ipsum sapiente esse, molestiae qui assumenda hic distinctio expedita blanditiis.
                    </p>
                    </div>
                </div>
                <div class="chat receive">
                    <img src="./assets/img/avatar.png" alt="">
                    <div class="details">
                    <p>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Nesciunt, optio maxime possimus sunt, a maiores ipsam enim animi quia exercitationem ipsum sapiente esse, molestiae qui assumenda hic distinctio expedita blanditiis.
                    </p>
                    </div>
                </div>
            </div>
            <form action="#" class="typing-area">
                <input type="text" placeholder="Ecriver ici...">
                <button><i class="fa fa-telegram"></i></button>
            </form>
        </section>
    </div>
</body>
</html>