<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MsgInst</title> 
    <link rel="stylesheet" href="./assets/css/style.css">
    <link rel="stylesheet" href="./assets/js/index.js">
    <link rel="stylesheet" href="https://use.fontawesome.com/5ee0f63a65.css">

</head>
<body>
    <div class="wraper">
        <section class="users">
            <header>
                <div class="content">
                    <img src="./assets/img/avatar.png" alt="">
                    <div class="details">
                        <span>User</span>
                        <p>En ligne</p>
                    </div>
                </div>
                <a href="" class="logout">déconnexion</a>
            </header>
            <div class="search">
                <span class="text">Discutons</span>
                <input type="text" placeholder="Entrez le nom a chercher">
                <button> 
                    <i class="fa fa-search"></i>
                </button>
            </div>
            <div class="users-list">
                <a href="#">
                    <div class="content">
                        <img src="./assets/img/avatar.png" alt="">
                        <div class="details">
                            <span>User</span>
                            <p>Message</p>
                        </div>
                    </div>
                    <div class="statut-dot">
                        <i class="fa fa-circle"></i>
                    </div>
                </a>
                <a href="#">
                    <div class="content">
                        <img src="./assets/img/avatar.png" alt="">
                        <div class="details">
                            <span>User</span>
                            <p>Message</p>
                        </div>
                    </div>
                    <div class="statut-dot">
                        <i class="fa fa-circle"></i>
                    </div>
                </a>
                <a href="#">
                    <div class="content">
                        <img src="./assets/img/avatar.png" alt="">
                        <div class="details">
                            <span>User</span>
                            <p>Message</p>
                        </div>
                    </div>
                    <div class="statut-dot">
                        <i class="fa fa-circle"></i>
                    </div>
                </a>
                <a href="#">
                    <div class="content">
                        <img src="./assets/img/avatar.png" alt="">
                        <div class="details">
                            <span>User</span>
                            <p>Message</p>
                        </div>
                    </div>
                    <div class="statut-dot">
                        <i class="fa fa-circle"></i>
                    </div>
                </a>
                <a href="#">
                    <div class="content">
                        <img src="./assets/img/avatar.png" alt="">
                        <div class="details">
                            <span>User</span>
                            <p>Message</p>
                        </div>
                    </div>
                    <div class="statut-dot">
                        <i class="fa fa-circle"></i>
                    </div>
                </a>
                <a href="#">
                    <div class="content">
                        <img src="./assets/img/avatar.png" alt="">
                        <div class="details">
                            <span>User</span>
                            <p>Message</p>
                        </div>
                    </div>
                    <div class="statut-dot">
                        <i class="fa fa-circle"></i>
                    </div>
                </a>
                <a href="#">
                    <div class="content">
                        <img src="./assets/img/avatar.png" alt="">
                        <div class="details">
                            <span>User</span>
                            <p>Message</p>
                        </div>
                    </div>
                    <div class="statut-dot">
                        <i class="fa fa-circle"></i>
                    </div>
                </a>
                <a href="#">
                    <div class="content">
                        <img src="./assets/img/avatar.png" alt="">
                        <div class="details">
                            <span>User</span>
                            <p>Message</p>
                        </div>
                    </div>
                    <div class="statut-dot">
                        <i class="fa fa-circle"></i>
                    </div>
                </a>
                <a href="#">
                    <div class="content">
                        <img src="./assets/img/avatar.png" alt="">
                        <div class="details">
                            <span>User</span>
                            <p>Message</p>
                        </div>
                    </div>
                    <div class="statut-dot">
                        <i class="fa fa-circle"></i>
                    </div>
                </a>
                <a href="#">
                    <div class="content">
                        <img src="./assets/img/avatar.png" alt="">
                        <div class="details">
                            <span>User</span>
                            <p>Message</p>
                        </div>
                    </div>
                    <div class="statut-dot">
                        <i class="fa fa-circle"></i>
                    </div>
                </a>
                <a href="#">
                    <div class="content">
                        <img src="./assets/img/avatar.png" alt="">
                        <div class="details">
                            <span>User</span>
                            <p>Message</p>
                        </div>
                    </div>
                    <div class="statut-dot">
                        <i class="fa fa-circle"></i>
                    </div>
                </a>
            </div>
        </section>
    </div>
</body>
<footer>
    <script src="./assets/js/main.js"></script>
</footer>
</html>